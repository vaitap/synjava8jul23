Lab10 - import java.util.List;

        public class Lab10 {
        public static void main(String[] args) {
            List<Emp> list =Emp.getList(100);
            list.stream().skip(10).limit(10).forEach(System.out::println);
        // 10 records should sorted according to project
        }
        }

Lab 9 - 
        import java.util.List;
        import java.util.Map;
        import java.util.stream.Collectors;

        public class Lab9 {
        public static void main(String[] args) {
            List<Emp> list = Emp.getList(10);
            list.forEach(System.out::println);
            List<String> listnames = list.stream().map(Emp::getEname).distinct().collect(Collectors.toList());
            System.out.println(listnames);

            // Map of department name and number of people working in that department
            // Compute sum of salaries by department
            Map<String, Double> totalByDept
                = list.stream()
                        .collect(Collectors.groupingBy(Emp::getDept, Collectors.summingDouble(Emp::getSalary)));
            System.out.println("Compute sum of salaries by department'");
            System.out.println(totalByDept);
            // sum of salaries per department
            // Map of department name and list of people working in that department
            Map<String, List<Emp>> byDept
            = list.stream()
                        .collect(Collectors.groupingBy(Emp::getDept));
            System.out.println("Map of department name and list of people working in that department");
            byDept.forEach((key,value)->System.out.println(key + " : " + value ));

            // Map of project name and number of people working in that project
            //counting()
	                // Map of department name and number of people working in that department 
                   // Map of project name and number of people working in that project 
            // Maxby/minby
                    // minimum salary per project
                    // maximum salary per department




        }
        }

Lab8 - Distinct and recap
    Create Lab8.java
        import java.util.List;

        public class Lab8 {
        public static void main(String[] args) {
            List<Emp> list =Emp.getList(100);
            list.forEach(System.out::println);
            System.out.println("Unique/Distinct Names ");
            list.stream()
                //	.map(e->e.getEname())
                    .map(Emp::getEname)
                    .distinct()
                    .forEach(System.out::println);
            System.out.println("Max of Salaries for Department fin ");
            
            System.out.println("Sum of Salaries for Project proj1 ");
            
            System.out.println("Max  Salary for Employee Name 'Saloni' ");
            
        }
        }

    Create emp.java
        import java.util.ArrayList;
        import java.util.List;

        public class Emp  {
            private int empno;
            private String ename;
            private double salary;
            private String project; // proj1, proj2, proj3
            private String dept; // HR, Fin, LT
            

            public static List<Emp> getList(int no){
                List<Emp> list = new ArrayList<Emp>();
                String[] names = {  "Vaishali", "Saloni","Vishal","Imran","Simran","Soni","Seema"	};
                String[] projects = {"proj1","proj2","proj3","proj4"};
                String[] depts = {"hr", "fin", "lt" };
                for (int i = 1;i <= no; i++) {
                    Emp e1 =new Emp();
                    e1.setEmpno(i);
                    e1.setEname(names[i%names.length]);
                    e1.setProject(projects[i % projects.length]);
                    e1.setDept(depts[i % depts.length]);
                    e1.setSalary((int)(Math.random()*1000));
                    list.add(e1);
                }
                return list;
            }
        }


Lab7 - reduce
            // Create list of names( 5/6 names) print sorted values
            List<String> listOfName = Arrays.asList("Anuja", "Priyanka", "Vaishali", "Sachin", "Bhawana", "Khushi");
            // List<String> listOfName = Arrays.asList("aa", "bb","cc");
            Optional<String> str = listOfName.stream().reduce((s, s1) -> s + s1);
            str.ifPresent(s -> System.out.println("Reduce returned " + s));
            //OptionalInt sum = listOfName.stream().mapToInt(s -> s.length()).reduce((s, s1) -> s + s1);
            int sum = listOfName.stream().mapToInt(s -> s.length()).sum();
            str.ifPresent(s -> System.out.println("Reduce returned " + sum));

Lab6 - map 
		
		//Create list of names( 5/6 names) print sorted values 
		List<String> listOfName = Arrays.asList("Anuja", "Priyanka","Vaishali","Sachin","Bhawana","Khushi");
		System.out.println("Map from String to String");
		listOfName.stream().map(s->s.toUpperCase()).forEach(System.out::println);
		System.out.println("Map from String to Integer");
		listOfName.stream().map(s->s.length()).forEach(System.out::println);
		System.out.println("Sum of Length of names : " + listOfName.stream().mapToInt(s->s.length()).sum());

Lab5 - Peek for debugging and understanding sequence of execution
     listOfName
	     			.stream()
	     			.peek(s->System.out.println("before filter" +s ))
	     			.filter(s->s.endsWith("i"))
	     			.forEach(System.out::println);
Lab4 - Create a list of 10 random numbers (1..100)
        Sort -> natural, revers and print
        Filter  < 70
        Filter  > 50
        Filter > 50 and < 70
        Show true if any number matches criteria -> 55 
        Show true if all the numbers are more than 12

Lab3 - import java.util.Arrays;
        import java.util.List;
        import java.util.Optional;

        public class Lab3 {

            public static void main(String[] args) {
                
                List<String> listOfName = Arrays.asList("Anuja", "Bishal","Vaishali","Sachin","Bhawana","Saloni", "Sani");
                System.out.println("Filtered List for name starting with S");
                listOfName.stream()
                        .filter(s->s.startsWith("S"))
                        .forEach(System.out::println);
            //-------------------------------
                Optional<String> nameendss = listOfName.stream()
                .filter(s->s.endsWith("ni"))
                .findFirst();
                System.out.println("Filter for name ending with ni and print first name " + nameendss );
            }
        }
    Modify Lab3.java to include
        1. list names having "sh"
        2. list names where length is more than 5
    

Lab2 - Create list of names( 5/6 names)
        print sorted values 
Lab1 - 
        1. Create integer stream and try foreach twice -> note error
        2. Create List of Strings -> list.stream(). -> process many times (stream() will give you new pointer)
     
    import java.util.ArrayList;
    import java.util.List;
    import java.util.stream.Stream;

    public class Lab1 {

        public static void main(String[] args) {
            Stream<Integer> strint = Stream.of(10,5,7,53,77,66,1);
    //		strint.forEach(System.out::println);
            Stream<String> strstr = Stream.of("aa","bb","vaishali","seema","amar", "amey","saloni", "zana");
            List<String> l1 = new ArrayList<String>();
            l1.add("aa"); l1.add("bb");l1.add("bbb");
            strint.forEach(System.out::println);
            strstr.forEach(System.out::println);
            l1.stream().forEach(System.out::println);        
        }

    }
